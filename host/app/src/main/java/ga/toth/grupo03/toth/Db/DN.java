package ga.toth.grupo03.toth.Db;

import android.net.Uri;

public class DN {
    public static final String DB_NAME = "chats.db";
    public static final int DB_VERSION = 1;

    // Constantes del content provider
    public static final String AUTHORITY = "ga.toth.grupo03.toth";
    public static final int STATUS_ITEM = 1;
    public static final int STATUS_DIR = 2;

    public class Ban {
        public static final String TABLE = "ban";
        public static final String USER = "_id";
        public static final String DEFAULT_SORT = USER + "DESC";
        public final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE);
    }

    public class Usr {
        public static final String TABLE = "users";
        public static final String USER = "_id";
        public static final String CPRIV = "certprivate";   //Certificado propio
        public static final String CPUB = "certpub";        //Certificado ajeno
        public static final String DEFAULT_SORT = USER + "DESC";
        public final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE);
    }

    public class Msg {
        public static final String TABLE = "messages";
        public static final String USER = "_id";
        public static final String MESSAGE = "message";
        public static final String TIME = "time";
        public static final String SEND = "send";
        public static final String IMG = "img";
        public static final String SERVER = "server";
        public static final String INDEX = "ind_tcreated";
        public static final String DEFAULT_SORT = TIME + "DESC";
        public final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE);
    }
}