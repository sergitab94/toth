package ga.toth.grupo03.toth.Db;

public class Msg {
    String username, message;
    int tcreated;
    Boolean send, img;

    public Msg(){}

    public Msg(String username, String message, int tcreated, boolean send, boolean img){
        this.username = username;
        this.message = message;
        this.tcreated = tcreated;
        this.send = send;
        this.img = img;
    }
}