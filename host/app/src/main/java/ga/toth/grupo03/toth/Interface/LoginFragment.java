package ga.toth.grupo03.toth.Interface;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import ga.toth.grupo03.toth.R;

public class LoginFragment extends Fragment implements View.OnClickListener, TextWatcher {

    private Button login, register;
    private EditText username, userpass;
    private TransitionDrawable trans;
    private Drawable bck[];
    private int position, ptrans = 0;
    private Handler mHandler = null;
    private static int REQUEST_EXIT = 100;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        login = (Button) view.findViewById(R.id.loglog);
        register = (Button) view.findViewById(R.id.logreg);
        username = (EditText) view.findViewById(R.id.loguser);
        userpass = (EditText) view.findViewById(R.id.logpass);

        login.setOnClickListener(this);
        register.setOnClickListener(this);

        Context context = container.getContext();

        // Add Draws
        bck = new Drawable[]{
                mirr(context, R.drawable.fondo1),
                mirr(context, R.drawable.fondo2),
                mirr(context, R.drawable.fondo3)};

        Drawable draw = ContextCompat.getDrawable(context, R.drawable.transition);
        trans = (TransitionDrawable) draw;
        view.setBackground(draw);
        return view;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mHandler = null;
    }

    @Override
    public void onStop(){
        super.onStop();
        mHandler = null;
    }

    @Override
    public void onResume(){
        super.onResume();
        if (mHandler == null) {
            mHandler = new Handler();
            mHandler.postDelayed(mRunnable, 10000);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == login){
            login();
        } else if(v == register) {
            startActivityForResult(new Intent(getActivity(), RegisterActivity.class), REQUEST_EXIT);
        }
    }

    private void login(){
        getActivity().finish();
        startActivity(new Intent(getActivity(), ChatlistActivity.class));
    }

    private Drawable mirr(Context context, int drawr){
        BitmapDrawable draw = (BitmapDrawable) ContextCompat.getDrawable(context, drawr);
        draw.setTileModeXY(Shader.TileMode.MIRROR, Shader.TileMode.MIRROR);
        return (Drawable) draw;
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
        ptrans = (ptrans == 1)? 0 : 1;
        position = (position+1) % bck.length;
        trans.setDrawable(ptrans, bck[position]);
        trans.reverseTransition(2000);
        if (mHandler!= null) {
            mHandler.postDelayed(mRunnable, 10000);
        }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_EXIT) {
            if (resultCode == Activity.RESULT_OK) {
                getActivity().finish();
            }
        }
    }
}