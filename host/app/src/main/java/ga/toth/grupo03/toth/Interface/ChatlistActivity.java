package ga.toth.grupo03.toth.Interface;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import ga.toth.grupo03.toth.Db.ListUser;
import ga.toth.grupo03.toth.Db.DbHelper;

import ga.toth.grupo03.toth.R;

public class ChatlistActivity extends AppCompatActivity implements View.OnClickListener, MenuItem.OnMenuItemClickListener,
        AdapterView.OnItemClickListener, TextWatcher {

    private MenuItem search, delete, settings;
    private FloatingActionButton add;
    private ListView list;
    private DbHelper cu;
    private Boolean bsearch = false;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_chatlist);

        //Float
        add = (FloatingActionButton) findViewById(R.id.fab);
        list = (ListView) findViewById(R.id.listuser);

        cu = new DbHelper(this);
        Cursor listUser =cu.getAllUser();
        if (listUser.getCount() <= 0) {
            Log.d("ñú", "Es nulo"); //Pendiente de poner un mensaje
        }

        ListUser cu = new ListUser(this, listUser);
        list.setAdapter(cu);
        list.setOnItemClickListener(this);
        add.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        //Menu
        search = menu.findItem(R.id.action_search);
        delete = menu.findItem(R.id.action_delete);
        settings = menu.findItem(R.id.action_settings);

        search.setOnMenuItemClickListener(this);
        delete.setOnMenuItemClickListener(this);
        settings.setOnMenuItemClickListener(this);

        search.setVisible(false);
        delete.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return false;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onClick(View v) {
        if(v == add){
            search.setVisible(true);
            delete.setVisible(false);
            settings.setVisible(false);
            bsearch = true;
        }
    }

    @Override
    public void onBackPressed() {
        if (!bsearch) {
            finish();
            return;
        }
        search.setVisible(false);
        delete.setVisible(false);
        settings.setVisible(true);
        bsearch = false;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String user = ((ListUser.ViewUser) view.getTag()).getUsername();

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("username", user);
        startActivity(intent);
    }
}