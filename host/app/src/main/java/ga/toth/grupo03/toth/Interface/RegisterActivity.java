package ga.toth.grupo03.toth.Interface;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;

import ga.toth.grupo03.toth.Interface.ChatlistActivity;
import ga.toth.grupo03.toth.R;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {
    private Button register;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_register);

        register = (Button) findViewById(R.id.regreg);

        register.setOnClickListener(this);
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onClick(View v) {
        setResult(RESULT_OK);
        finish();
        startActivity(new Intent(this, ChatlistActivity.class));
    }
}
