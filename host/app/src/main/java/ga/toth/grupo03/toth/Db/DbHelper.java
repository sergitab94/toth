package ga.toth.grupo03.toth.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {
    private static final String TAG = DbHelper.class.getSimpleName();

    public DbHelper(Context context) {
        super(context, DN.DB_NAME, null, DN.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = String.format("create table %s (%s text primary key not null)",
                DN.Ban.TABLE,
                DN.Ban.USER);
        Log.d(TAG, "onCreate con SQL: " + sql);
        db.execSQL(sql);

        sql = String.format("create table %s (%s text primary key not null, %s text not null, %s text)",
                DN.Usr.TABLE,
                DN.Usr.USER,
                DN.Usr.CPRIV,
                DN.Usr.CPUB);
        Log.d(TAG, "onCreate con SQL: " + sql);
        db.execSQL(sql);

        sql = String.format("create table %s (%s text not null, %s text not null, %s int not null, %s int not null, %s int not null, %s int not null)",
                DN.Msg.TABLE,
                DN.Msg.USER,
                DN.Msg.MESSAGE,
                DN.Msg.TIME,
                DN.Msg.SEND,
                DN.Msg.IMG,
                DN.Msg.SERVER);
        Log.d(TAG, "onCreate con SQL: " + sql);
        db.execSQL(sql);

        sql = String.format("CREATE INDEX %s ON %s(%s)",
                DN.Msg.INDEX,
                DN.Msg.TABLE,
                DN.Msg.TIME);
        Log.d(TAG, "onCreate con SQL: " + sql);
        db.execSQL(sql);
    }

    // Llamado siempre que tengamos una nueva version
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + DN.Ban.TABLE);
        db.execSQL("drop table if exists " + DN.Usr.TABLE);
        db.execSQL("drop table if exists " + DN.Msg.TABLE);
        onCreate(db);
        Log.d(TAG, "onUpgrade");
    }

    public int createUser(User e){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues val = new ContentValues();
        val.put(DN.Usr.USER, e.username);
        val.put(DN.Usr.CPRIV, e.certprivate);
        val.put(DN.Usr.CPUB, e.certpub);

        int i = (int) db.insert(DN.Usr.TABLE, null, val);
        db.close();
        return i;
    }

    public User getUser(String username){
        SQLiteDatabase db = getWritableDatabase();

        Cursor c = db.query(DN.Usr.TABLE, new String[]{DN.Usr.USER, DN.Usr.CPRIV, DN.Usr.CPUB}, DN.Usr.USER+"=?", new String[]{username}, null, null, null);

        if (c == null)  return null;

        c.moveToFirst();
        db.close();
        return new User(c.getString(c.getColumnIndex(DN.Usr.USER)), c.getString(c.getColumnIndex(DN.Usr.CPRIV)), c.getString(c.getColumnIndex(DN.Usr.CPUB)));
    }

    public Cursor getAllUser(){
        SQLiteDatabase db = getWritableDatabase();

        Cursor c = db.rawQuery("SELECT " + DN.Usr.TABLE + ".*  FROM " + DN.Usr.TABLE + " LEFT JOIN " + DN.Msg.TABLE +
                " ON " + DN.Usr.TABLE + "." + DN.Usr.USER + "=" + DN.Usr.TABLE + "." + DN.Msg.USER +
                " GROUP BY " + DN.Usr.TABLE + "." + DN.Msg.USER +
                " ORDER BY " + DN.Msg.TIME + " ASC;", null);

        if (c == null)  return null;

        c.moveToFirst();
        db.close();
        return c;
    }

    public int updateUser(){
        return 0;
    }

    public int deleteUser(String username){
        SQLiteDatabase db = getWritableDatabase();

        int i = db.delete(DN.Usr.TABLE, DN.Usr.USER + "=?", new String[]{username});
        db.close();
        return i;
    }

    public Cursor getMessages(String username, int offset){
        SQLiteDatabase db = getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM (SELECT *  FROM " + DN.Msg.TABLE +
                " WHERE " + DN.Msg.USER + " = '" + username +
                "' ORDER BY " + DN.Msg.TIME + " DESC LIMIT 10 OFFSET " + offset +
                ") ORDER BY " + DN.Msg.TIME + " ASC;", null);

        if (c == null)  return null;

        c.moveToFirst();
        db.close();
        return c;
    }

    public int deleteMessages(String username, int offset){
        return 0;
    }

    public Cursor getBan(String username, int offset){
        SQLiteDatabase db = getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM " + DN.Ban.TABLE + ";", null);

        if (c == null)  return null;

        c.moveToFirst();
        db.close();
        return c;
    }

    public int deleteBan(String username, int offset){
        return 0;
    }
}