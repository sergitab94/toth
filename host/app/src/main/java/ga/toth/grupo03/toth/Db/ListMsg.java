package ga.toth.grupo03.toth.Db;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Environment;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import ga.toth.grupo03.toth.R;

public class ListMsg extends CursorAdapter {

    public class ViewMsg{
        private LinearLayout bubble;
        private View right;
        private TextView msg;
        private ImageView img;
        private TextView date;
        private ImageView clock;
        private ImageView check;
    }

    public ListMsg(Context context, Cursor cursor){
        super(context, cursor, FLAG_REGISTER_CONTENT_OBSERVER);
    }



    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewMsg vm = new ViewMsg();
        View v = inflater.inflate(R.layout.msg, parent, false);

        vm.bubble = (LinearLayout) v.findViewById(R.id.bubble);
        vm.right = (View) v.findViewById(R.id.rightmsg);
        vm.msg = (TextView) v.findViewById(R.id.sndmsg);
        vm.img = (ImageView) v.findViewById(R.id.imgsnd);
        vm.date = (TextView) v.findViewById(R.id.datesnd);
        vm.clock = (ImageView) v.findViewById(R.id.clock);
        vm.check = (ImageView) v.findViewById(R.id.check);

        v.setTag(vm);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewMsg vm = (ViewMsg) view.getTag();

        Boolean img = (cursor.getInt(cursor.getColumnIndex(DN.Msg.IMG)) == 1);
        Boolean send = (cursor.getInt(cursor.getColumnIndex(DN.Msg.SEND)) == 1);
        Boolean server = (cursor.getInt(cursor.getColumnIndex(DN.Msg.SERVER)) == 1);


        if(send) {
            vm.right.setVisibility(View.VISIBLE);
            vm.bubble.setBackgroundTintList(context.getResources().getColorStateList(R.color.snd,context.getTheme()));
            if (!server){
                vm.clock.setVisibility(View.GONE);
                vm.check.setVisibility(View.VISIBLE);
            } else {
                vm.clock.setVisibility(View.VISIBLE);
                vm.check.setVisibility(View.GONE);
            }
        } else {
            vm.right.setVisibility(View.GONE);
            vm.bubble.setBackgroundTintList(context.getResources().getColorStateList(R.color.rcv,context.getTheme()));
            vm.clock.setVisibility(View.GONE);
            vm.check.setVisibility(View.GONE);
        }

        if(img){
            /*File sd = Environment.getExternalStorageDirectory();
            File image = new File(sd + "Toth/", cursor.getString(cursor.getColumnIndex(DN.Msg.MESSAGE)));
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(),bmOptions);
            vm.img.setImageBitmap(bitmap);*/
            vm.img.setVisibility(View.VISIBLE);
            vm.msg.setVisibility(View.GONE);
        } else {
            vm.msg.setText(cursor.getString(cursor.getColumnIndex(DN.Msg.MESSAGE)));
            vm.img.setVisibility(View.GONE);
            vm.msg.setVisibility(View.VISIBLE);
        }

        Date dated = new Date(cursor.getLong(cursor.getColumnIndex(DN.Msg.TIME))*1000);
        SimpleDateFormat jdf = new SimpleDateFormat("HH:mm - dd-MM-yyyy");
        vm.date.setText(jdf.format(dated));
    }

}