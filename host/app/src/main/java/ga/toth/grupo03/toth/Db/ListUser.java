package ga.toth.grupo03.toth.Db;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import ga.toth.grupo03.toth.R;

public class ListUser extends CursorAdapter {

    public class ViewUser{
        private TextView txtFirst;
        private TextView txtName;
        private TextView txtInfor;

        public String getUsername(){return txtName.getText().toString();}
    }

    public ListUser(Context context, Cursor cursor){
        super(context, cursor, false);
    }

    private int background(char letter){
        int[] color = new int[]{
                Color.YELLOW,
                Color.RED,
                Color.GREEN,
                Color.GRAY,
                Color.BLUE,
                Color.MAGENTA
        };

        return color[letter%color.length];
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        Log.d("baaa", "ejecutado");

        View v = inflater.inflate(R.layout.usr, parent, false);

        ViewUser vu = new ViewUser();
        vu.txtFirst = (TextView) v.findViewById(R.id.txtFirst);
        vu.txtName = (TextView) v.findViewById(R.id.txtName);
        vu.txtInfor = (TextView) v.findViewById(R.id.txtInfor);
        v.setTag(vu);

        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewUser vu = (ViewUser) view.getTag();

        String username = cursor.getString(cursor.getColumnIndex(DN.Usr.USER));
        char letter = Character.toUpperCase(username.charAt(0));
        vu.txtFirst.setText(letter+"");
        vu.txtFirst.setBackgroundColor(background(letter));
        vu.txtName.setText(username);
        vu.txtInfor.setText("nada de nada");
    }
}
