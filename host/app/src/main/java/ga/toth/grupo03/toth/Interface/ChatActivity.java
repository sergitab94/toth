package ga.toth.grupo03.toth.Interface;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ListIterator;

import ga.toth.grupo03.toth.Db.DbHelper;
import ga.toth.grupo03.toth.Db.ListMsg;
import ga.toth.grupo03.toth.Db.ListUser;
import ga.toth.grupo03.toth.R;
import ga.toth.grupo03.toth.Db.User;

public class ChatActivity extends AppCompatActivity implements MenuItem.OnMenuItemClickListener, AbsListView.OnScrollListener {
    User user;
    MenuItem delete;
    ListView list;
    DbHelper cu;
    int punt, move;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_chat);
        String us = getIntent().getStringExtra("username");

        list = (ListView) findViewById(R.id.listchat);

        cu = new DbHelper(this);

        punt = 10; move = 10;
        //Obtener de la base de datos
        Cursor listmsg = cu.getMessages(us, 0);

        if (listmsg.getCount() <= 0) {
            Log.d("ñú", "Es nulo"); //Pendiente de poner un mensaje
        } else {
            ListMsg cu = new ListMsg(this, listmsg);
            list.setAdapter(cu);
            list.setOnScrollListener(this);
        }

    }


    // Sólo si se permite borrar mensaje
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        menu.removeItem(R.id.action_search);
        menu.removeItem(R.id.action_settings);

        delete = menu.findItem(R.id.action_delete);
        delete.setOnMenuItemClickListener(this);
        delete.setVisible(false);

        return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        return false;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        Log.d("prueba", "has llegado");
    }
}