package ga.toth.grupo03.toth.Interface;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ga.toth.grupo03.toth.Db.DbHelper;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LoginFragment fragment = new LoginFragment();
        getFragmentManager().beginTransaction().add(android.R.id.content,
                fragment, fragment.getClass().getSimpleName()).commit();
        getSupportActionBar().hide();

        // Código a eliminar de la creación de la base de datos
        DbHelper dbHelper = new DbHelper(this.getBaseContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();


    }
}