package ga.toth.grupo03.toth.Interface;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.security.KeyStore;

import javax.crypto.Cipher;

import ga.toth.grupo03.toth.R;


public class AccessFragment extends Fragment implements View.OnClickListener{

    private Button accbutton;
    private EditText accpass;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_access, container, false);
        super.onCreate(savedInstanceState);

        accbutton = (Button) view.findViewById(R.id.accbutton);
        accpass = (EditText) view.findViewById(R.id.accpass);

        accbutton.setOnClickListener(this);

        Context context = container.getContext();
        Point size = new Point();
        (getActivity().getWindowManager().getDefaultDisplay()).getSize(size);

        return view;
    }

    @Override
    public void onClick(View v) {

    }

}
