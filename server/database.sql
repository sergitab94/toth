CREATE DATABASE toth;

USE toth;

CREATE TABLE users(
    username VARCHAR(40) PRIMARY KEY,
    password VARCHAR(255) NOT NULL,
    configtime INT(11) NOT NULL,
    timedrop INT(11) NOT NULL,
    tokenuser VARCHAR(8) UNIQUE NOT NULL,
    tokenpass VARCHAR(255) NOT NULL
);

CREATE TABLE message(
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    tokenuser VARCHAR(40),
    usersend VARCHAR(40) NOT NULL,
    text LONGTEXT NOT NULL,
    iscert BOOLEAN NOT NULL,
    FOREIGN KEY (tokenuser) REFERENCES users(tokenuser) ON DELETE CASCADE ON UPDATE CASCADE
);