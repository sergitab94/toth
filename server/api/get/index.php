<?php
require_once __DIR__ . "/../db/db.php";
ini_set("session.use_cookies", 0);
ini_set("session.use_only_cookies", 0);
ini_set("session.use_trans_sid", 1);
ini_set("session.cache_limiter", "");
session_start();
header('Content-Type: application/json');

# Check if user is banned
if (!empty($_SESSION['try']) && $_SESSION['try']>2) {
    if (time()<$_SESSION['try']){
        echo '{"error": "timeout"}';
        die();
    } else unset($_SESSION['try']);
}

# Block MiTM
if ((!empty($_SESSION['address']) && $_SESSION['address'] !== $_SERVER['REMOTE_ADDR'])){
	$_SESSION['try'] = time() + 36000;
    echo '{"error": "timeout"}' ;
    die();
}
if (empty($_SESSION['address'])) $_SESSION['address'] = $_SERVER['REMOTE_ADDR'];
$db = new db();

if(!isset($_GET["type"])) ban();
switch ($_GET["type"]) {
    case "new":
        if(!isset($_GET["username"], $_GET["password"], $_GET["configtime"])) ban();
        $msg["username"] = strtolower($msg["username"]);
        if(!preg_match('/^[a-z0-9-_]{5,40}$/', $_GET["username"])) ban();
        if(!preg_match('/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,16}$/', $_GET["password"])) ban();
        if(!preg_match('/^[0-9]+$/', $_GET["configtime"])) ban();
        if(intval($_GET["configtime"])<1500 || intval($_GET["configtime"])>63113850) ban();
        
        $sql = $db->newUser($_GET["username"], $_GET["password"], $_GET["configtime"]);
        if($sql === false) ban();
        else if($sql === "failed") echo '{"sys": "ko"}';
        else echo json_encode($sql);
        break;
    case "upass":
        if(!isset($_GET["tokenuser"], $_GET["tokenpass"], $_GET["passwordold"], $_GET["passwordnew"])) ban();
        if(!preg_match('/^[A-Za-z0-9]{8}$/', $_GET["tokenuser"])) ban();
        if(!preg_match('/^[A-Za-z0-9]{32}$/', $_GET["tokenpass"])) ban();
        if(!preg_match('/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,16}$/', $_GET["passwordold"])) ban();
        if(!preg_match('/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,16}$/', $_GET["passwordnew"])) ban();
        
        if(!$db->upass($_GET["tokenuser"], $_GET["tokenpass"], $_GET["passwordold"], $_GET["passwordnew"])) ban();
        echo '{"sys": "ok"}';
        break;
    case "udrop":
        if(!isset($_GET["tokenuser"], $_GET["tokenpass"], $_GET["configtime"])) ban();
        if(!preg_match('/^[A-Za-z0-9]{8}$/', $_GET["tokenuser"])) ban();
        if(!preg_match('/^[A-Za-z0-9]{32}$/', $_GET["tokenpass"])) ban();
        if(!preg_match('/^[0-9]+$/', $_GET["configtime"])) ban();
        if(intval($_GET["configtime"])<1500 || intval($_GET["configtime"])>63113850) ban();
        
        if(!$db->udrop($_GET["tokenuser"], $_GET["tokenpass"], $_GET["configtime"])) ban();
        echo '{"sys": "ok"}';
        break;
    case "newtoken":
        if(!isset($_GET["username"], $_GET["password"])) ban();
        $msg["username"] = strtolower($msg["username"]);
        if(!preg_match('/^[a-z0-9-_]{5,40}$/', $_GET["username"])) ban();
        if(!preg_match('/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,16}$/', $_GET["password"])) ban();
        
        $sql = $db->newtoken($_GET["username"], $_GET["password"]);
        if($sql === false) ban();
        echo json_encode($sql);
        break;
    case "deltoken":
        if(!isset($_GET["username"], $_GET["password"], $_GET["tokenuser"], $_GET["tokenpass"])) ban();
        $msg["username"] = strtolower($msg["username"]);
        if(!preg_match('/^[a-z0-9-_]{5,40}$/', $_GET["username"])) ban();
        if(!preg_match('/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,16}$/', $_GET["password"])) ban();
        if(!preg_match('/^[A-Za-z0-9]{8}$/', $_GET["tokenuser"])) ban();
        if(!preg_match('/^[A-Za-z0-9]{32}$/', $_GET["tokenpass"])) ban();
        
        if(!$db->deltoken($_GET["username"], $_GET["password"], $_GET["tokenuser"], $_GET["tokenpass"])) ban();
        echo '{"sys": "ok"}';
        break;
    case "del":
        if(!isset($_GET["username"], $_GET["password"], $_GET["tokenuser"], $_GET["tokenpass"])) ban();
        $msg["username"] = strtolower($msg["username"]);
        if(!preg_match('/^[a-z0-9-_]{5,40}$/', $_GET["username"])) ban();
        if(!preg_match('/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,16}$/', $_GET["password"])) ban();
        if(!preg_match('/^[A-Za-z0-9]{8}$/', $_GET["tokenuser"])) ban();
        if(!preg_match('/^[A-Za-z0-9]{32}$/', $_GET["tokenpass"])) ban();
        
        if(!$db->del($_GET["username"], $_GET["password"], $_GET["tokenuser"], $_GET["tokenpass"])) ban();
        echo '{"sys": "ok"}';
        break;
    default:
        ban();
}

/**
 * Ban request
 *
 */
function ban() {
    if(!isset($_SESSION["try"])) $_SESSION["try"] = 0;
    else $_SESSION["try"]++;

    if($_SESSION["try"]>2) {
        $_SESSION['try'] = time() + 1800;
        echo '{"error": "timeout"}';
    } else echo '{"error": "bad request"}';
    die();
}
?>