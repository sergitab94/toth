<?php
class con{
    /**
     * @param {Mixed} Connection to database
    */
    private $mysqli;

    /**
     * Create object to connect database
     *
     * @return {Mixed} Object database
     */
    public function __construct(){
        $CONFIG = parse_ini_file(__DIR__ . "/config.ini", true);
        
        $this->mysqli = new mysqli(
            $CONFIG['db_host'],
            $CONFIG['db_user'],
            $CONFIG['db_pass'],
            $CONFIG['db_database'],
            $CONFIG['db_port']
        );
        
        if ($this->mysqli->connect_error) die('Error de Conexión (' .
            $this->mysqli->connect_errno . ') ' . $this->mysqli->connect_error);

        if (!$this->mysqli->set_charset("utf8")) die("Error cargando el conjunto de caracteres utf8: ".
            $this->mysqli->error);
    }

    /**
     * Show data of database
     *
     * @param {String} Table of show
     * @param {Mixed} Filter results
     *
     * @return {Mixed} Results
     */
    public function select($table, $where=null){
        if (empty($table)) throw new Exception('Incorrect param query');
        $table = $this->mysqli->escape_string($table);
        $where = $this->secure($where);
        $sql = "SELECT * FROM $table";
        if ($where != null){
            $pieces=[];
            foreach($where as $key => $value){
                array_push($pieces, "`$key` = '$value'");
            }
            $sql .= " WHERE " . implode(" AND ", $pieces);
        }
        $sql .= ";";
        return $this->query($sql);
    }

    /**
     * Drop data of database
     *
     * @param {String} Table of show
     *    @param {Mixed} Filter results
     *
     * @return {Mixed} Results
     */
    public function delete($table, $where=null){
        if (empty($table)) throw new Exception('Incorrect param query');
        $table = $this->mysqli->escape_string($table);
        $sql = "DELETE FROM $table";
        if ($where != null){
            $pieces=[];
            foreach($where as $key => $value){
                $key = $this->mysqli->escape_string($key);
                $value[1] = $this->mysqli->escape_string($value[1]);
                array_push($pieces, "`$key` $value[0] '$value[1]'");
            }
            $sql .= " WHERE " . implode(" AND ", $pieces);
        }
        $sql .= ";";
        return $this->query($sql);
    }

    /**
     * Update data of database
     *
     * @param {String} Table of show
     * @param {String} Columns of update
     *    @param {Mixed} Filter results
     *
     * @return {Mixed} Results
     */
    public function update($table, $set, $where){
        if (empty($table)) throw new Exception('Incorrect param query');
        if (empty($set)) throw new Exception('Incorrect param query');
        if (empty($where)) throw new Exception('Incorrect param query');
        $table = $this->mysqli->escape_string($table);
        $set = $this->secure($set);
        $where = $this->secure($where);
        
        $sql = "UPDATE $table SET ";
        $pieces=[];
        foreach($set as $key => $value){
            array_push($pieces, "`$key` = '$value'");
        }
        $sql .= implode(", ", $pieces) . " WHERE ";
        
        $pieces=[];
        foreach($where as $key => $value){
            array_push($pieces, "`$key` = '$value'");
        }
        $sql .= implode(" AND ", $pieces) . ";";
        return $this->query($sql);
    }


    /**
     * Insert in table
     *
     * @param {String} Table to insert
     * @param {Mixed} Data to insert
     *
     */
    public function insert($table, $insert){
        if (empty($table)) throw new Exception("Param Insert incorrect query");
        if (empty($insert)) throw new Exception("Param Insert incorrect query");
        $table = $this->mysqli->escape_string($table);
        $insert = $this->secure($insert);
        
        $query="INSERT INTO `$table`";
        $data="";

        $query .= " (" . implode(", ", array_keys($insert)) . ") VALUES";
        $query .= " ('" . implode("', '", array_values($insert)) . "');";
        return $this->query($query);
    }


    /**
     * Query to database
     *
     * @param {String} Query to realized
     *
     * @return {Mixed} Data of query
     */
    private function query($sql){
        $return = array(
            "success" => false,
            "query" => null
        );
        
        $result = $this->mysqli->query($sql);
        
        if(is_bool($result)) $return["success"] = $result;
        else {
            $return["success"] = true;
            $return["query"] = $result->fetch_all(MYSQLI_ASSOC);
            $result->free();
        }
        return $return;
    }
    
    /**
     * Secure Arrays
     *
     * @param {Mixed} Escape Array strings
     *
     * @return {Mixed} Arrays escaped
     */
    private function secure($array){
        return array_map(array($this->mysqli, 'real_escape_string'), $array);
    }
    
    /**
     * Destruct object connection
     */
    public function __destruct(){
        $this->mysqli->close();
    }
}
?>