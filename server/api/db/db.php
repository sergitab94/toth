<?php
class db{

    private $con;

    /**
     * Create object database
     *
     * @return {Mixed} Object database
    */
    public function __construct(){
        include_once __DIR__ . "/conn.php";
        $this->con =  new con();
    }
    
    # =============================================================================
    # CREATE USER
    # =============================================================================

    /**
     * Create a new user
     *
     * @param {String} Name of user
     * @param {String} Password of user
     * @param {Integer} Time to auto destruct
     *
     * @return {Mixed} Array with results or boolean false if fails
     */
    public function newUser($username, $password, $configtime){
        if(!isset($username, $password, $configtime)) return false;
        $checkuser=$this->con->select("users", ["username"=>$username]);
        if(!$checkuser["success"]) return false;
        if(!empty($checkuser["query"])) return "failed";
        
        $tokenuser = $this->generateRandom(8);
        $checktoken = $this->con->select("users", ["tokenuser"=>$tokenuser]);
        while(!empty($checktoken["query"])){
            $tokenuser = $this->generateRandom(8);
            $checktoken = $this->con->select("users", ["tokenuser"=>$tokenuser]);
        }
        
        $tokenpass = $this->generateRandom(32);
        $data = ["username" => $username,
            "password" => $this->generateHash($password, $username),
            "configtime" => $configtime,
            "timedrop" => time()+$configtime,
            "tokenuser" => $tokenuser,
            "tokenpass" => $this->generateHash($tokenpass, $tokenuser)];
        $result = $this->con->insert("users", $data);
        if (!$result["success"]) return false;

        return ["tokenuser" => $tokenuser, "tokenpass" => $tokenpass];
    }

    # =============================================================================
    # READ USER
    # =============================================================================
    /**
     * Operations of connect one user in WebSocket (update time, send messages)
     *
     * @param {String} Token of user
     * @param {String} Token of pass
     *
     * @return {Mixed} Message of user to connect
     *
     */
    public function connectUser($tokenuser, $tokenpass){
        $user = $this->getUser($tokenuser, $tokenpass);
        if ($user === false) return false;
        
        $checkupdate = $this->con->update("users", ["timedrop"=>time()+$user[0]['configtime']] , ["tokenuser"=>$tokenuser]);
        if (!$checkupdate["success"]) return false;

        $msg=$this->con->select("message", ["tokenuser"=>$tokenuser]);
        if (!$msg["success"]) return false;
        return $msg["query"];
    }

    /**
     * Check if the userfrom exist
     *
     * @param {String} Token of user
     * @param {String} Token of pass
     * @param {String} Username of user
     * @param {String} Username to send message
     *
     * @return {Boolean} If parameters are correct return true
     */
    public function checkSend($tokenuser, $tokenpass, $username, $userfrom){
        $user = $this->getUser($tokenuser, $tokenpass);
        if ($user === false) return false;
        if ($user[0]["username"]!==$username) return false;
        
        $checkusersend = $this->con->select("users", ["username"=>$userfrom]);
        if (!$checkusersend["success"] || empty($checkusersend["query"])) return "failed";
        return $user[0]["username"];
    }
    
    /**
     * Get user if exist (with usertoken)
     *
     * @param {String} Token of user
     * @param {String} Token of pass
     *
     * @return {Mixed} User or fails if not exist
     */
    public function getUser($tokenuser, $tokenpass){
        if (!isset($tokenuser, $tokenpass)) return false;
        $user = $this->con->select("users", ["tokenuser"=>$tokenuser]);
        if (!$user["success"] || empty($user["query"])) return false;
        if (!$this->checkHash($tokenpass, $tokenuser, $user["query"][0]['tokenpass'])) return false;
        return $user["query"];
    }
    
    /**
     * Get user if exist (with username)
     *
     * @param {String} Token of user
     * @param {String} Token of pass
     *
     * @return {Mixed} User or fails if not exist
     */
    private function getPass($username, $password){
        if (!isset($username, $password)) return false;
        $user = $this->con->select("users", ["username"=>$username]);
        if (!$user["success"] || empty($user["query"])) return false;
        if (!$this->checkHash($password, $username, $user["query"][0]['password'])) return false;
        return true;
    }
    
    # =============================================================================
    # UPDATE USER
    # =============================================================================
    /**
     * Update password of user
     *
     * @param {String} Token of user
     * @param {String} Token of password
     * @param {String} Password old of user
     * @param {String} Password new of user
     *
     * @return {Boolean} If update success true
     */
    public function upass($tokenuser, $tokenpass, $passwordold, $passwordnew){
        if (!isset($tokenuser, $tokenpass, $passwordold, $passwordnew)) return false;
        
        $user = $this->getUser($tokenuser, $tokenpass);
        if ($user === false) return false;
        
        $pass = $this->getPass($user[0]['username'], $passwordold);
        if ($pass === false) return false;
        
        $checkupdate = $this->con->update("users", ["password"=> $this->generateHash($passwordnew, $user[0]['username'])] , ["tokenuser"=>$tokenuser]);
        return $checkupdate["success"];
    }

    /**
     * Update configtime of user
     *
     * @param {String} Token of user
     * @param {String} Token of password
     * @param {String} Time of configure of delete
     *
     */
    public function udrop($tokenuser, $tokenpass, $configtime){
        if (!isset($tokenuser, $tokenpass, $configtime)) return false;
        
        $user = $this->getUser($tokenuser, $tokenpass);
        if ($user === false) return false;

        $checkupdate = $this->con->update("users", ["configtime"=>$configtime] , ["tokenuser"=>$tokenuser]);
        return $checkupdate["success"];
    }
    
    /**
     * Create new token for user
     *
     * @param {String} Name of user
     * @param {String} Password of user
     * 
     * @return {Mixed} Array token user and token password
     */
    public function newtoken($username, $userpass){
        if(!isset($username, $userpass)) return false;
        
        $pass = $this->getPass($username, $userpass);
        if ($pass === false) return false;
        
        $tokenuser = $this->generateRandom(8);
        $checktoken = $this->con->select("users", ["tokenuser"=>$tokenuser]);
        while(!empty($checktoken["query"])){
            $tokenuser = $this->generateRandom(8);
            $checktoken = $this->con->select("users", ["tokenuser"=>$tokenuser]);
        }
        $tokenpass = $this->generateRandom(32);

        $data = ["tokenuser" => $tokenuser,
            "tokenpass" => $this->generateHash($tokenpass, $tokenuser)];
        $checkupdate = $this->con->update("users", $data, ["username"=>$username]);
        if (!$checkupdate["success"]) return false;

        return ["tokenuser" => $tokenuser, "tokenpass" => $tokenpass];
    }
    
    # =============================================================================
    # DELETE USER
    # =============================================================================
    /**
     * Delete user specific
     *
     * @param {String} Username of user
     * @param {String} Password of user
     * @param {String} Usertoken of user
     * @param {String} Passwordtoken of user
     *
     * @return {Boolean} If delete successfully send true 
     */
    public function del($username, $userpass, $tokenuser, $tokenpass){
        if (!isset($username, $userpass, $tokenuser, $tokenpass)) return false;
        
        $user = $this->getUser($tokenuser, $tokenpass);
        if ($user === false) return false;
        
        $pass = $this->getPass($username, $userpass);
        if ($pass === false) return false;
        

        $del = $this->con->delete('users', ['username'=>['=',$username]]);
        return $del["success"];
    }
    
    /**
     * Delete all users old
     */
    public function deleteTime(){
        $this->con->delete('users', ['timedrop'=>['<',time()]]);
    }
    
    /**
     * Delete token for user
     * 
     * @param {String} Username of user
     * @param {String} Password of user
     * @param {String} Usertoken of user
     * @param {String} Passwordtoken of user
     *
     * @return {Boolean} If delete successfully send true 
     */
    public function deltoken($username, $userpass, $tokenuser, $tokenpass){
        if(!isset($username, $userpass, $tokenuser, $tokenpass)) return false;
        
        $user = $this->getUser($tokenuser, $tokenpass);
        if ($user === false) return false;
        
        if ($this->newtoken($username, $userpass) === false) return false;
        return true;
    } 
    
    # =============================================================================
    # CREATE AND DELETE MESSAGE
    # =============================================================================
    /**
     * Save message (include certificates)
     *
     * @param {String} User that send message
     * @param {String} Username to send message
     * @param {String} Message to send
     * @param {Boolean} Message is a cert
     *
     */
    public function saveMessage($userEmisor, $userReceptor, $message, $iscert){
        if(!isset($userEmisor, $userReceptor, $message, $iscert)) return;
        $qReceptor = $this->con->select("users", ["username"=>$userReceptor]);
        if (!$qReceptor["success"] || empty($qReceptor["query"])) return;
        
        $this->con->insert("message", ["tokenuser"=>$qReceptor["query"][0]["tokenuser"], "usersend"=>$userEmisor, "text"=>$message, "iscert"=>$iscert]);
    }
    
    /**
     * Delete Messages sends
     *
     * @param {String} User of delete messages
     *
     * @return {Boolean} If delete successfully
     */
    public function deleteMessage($id){
        if (!isset($id)) return false;
        $sql = $this->con->delete("message", ["id"=>["=", $id]]);
        if (!$sql["success"]) return false;
        return true;
    }
    
    # =============================================================================
    # GENERAL FUNCTIONS
    # =============================================================================
    /**
     * Random String
     *
     * @param {Integer} Length of string
     *
     * @return {String} String Random in Base62
     */
    private function generateRandom($length){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Generate a new hash
     *
     * @param {String} Text plain to hashing
     * @param {String} Text plain with salt
     *
     * @return {String} Hash string
     */
    private function generateHash($plain, $salt){
        return hash_hmac('sha512',$plain, $salt);
    }

    /**
     * Check if password is correctly
     *
     * @param {String} Password to checker
     * @param {String} Salt of password
     *
     * @return {Boolean} If is correct or not
     */
    private function checkHash($plain, $salt, $hash){
        $tempHash = $this->generateHash($plain, $salt);
        return hash_equals($hash, $tempHash);
    }
}
?>