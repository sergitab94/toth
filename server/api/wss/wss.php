<?php
class wss{

    private $db;
    private $socket;

    /**
     * Create WebServerSocket
     *
     */
    public function __construct($host, $port){
        require_once __DIR__ . "/../db/db.php";
        require_once __DIR__ . "/socket.php";
        $this->db = new db();
        $this->socket = new socket($host, $port);
        while (true) {
            try {
                $this->doServer();
            } catch (Exception $e) {
                echo 'Parece que algo ha ido mal: ',  $e->getMessage(), "\n";
            }
        }
        print("Websocket Detenido");
    }

    /**
     * Main tasks of server
     *
     */
    private function doServer(){
        $allmsg = $this->socket->getMessages();
        foreach ($allmsg as $msg){
            $this->dealMsg($msg["user"], $msg["msg"]);
        }
    }

    /**
     * Deal differents messages
     *
     * @param {Mixed} Data of user
     * @param {Mixed} Msg to process
     *
     */
    private function dealMsg($data, $msg){
        if (!$data["handshake"]){
            $this->connect($data,$msg);
            return;
        }
        if (!isset($msg["type"])) {
            $this->disconnect($data);
            return;
        }
        switch ($msg["type"]){
            case "cert":
            case "msg":
                $this->msg($data, $msg);
                break;
            case "disconnect":
                print('{"sys":"disconnect"}');
            default:
                $this->disconnect($data);
        }
    }
    
    /**
     * Handshake with login user
     *
     * @param {Mixed} Data of user
     * @param {Mixed} Msg to process
     *
     */
    private function connect($data, $msg){
        if($msg === false){
            $this->disconnect($data);
            return;
        }
        
        $user = $this->db->getUser($msg["tokenuser"], $msg["tokenpass"]);
        if ($user === false){
            $this->disconnect($data);
            return;
        }
        
        $this->socket->shake($data['resource'], $user[0]["username"], $msg["key"]);
        
        $smsg = $this->db->connectUser($msg["tokenuser"], $msg["tokenpass"]);
        if ($smsg === false) {
            $this->disconnect($data);
            return;
        }
        
        foreach ($smsg as $value){
            $usersend = $this->socket->userSocket($value["usersend"]);
            $type = ($value["iscert"])? "cert" : "msg" ;
            $rmsg = ["type" => $type,
                    "username" => $value["usersend"],
                    "data" =>  $value["text"]];
            $oksend = $this->socket->sendUsr($data["resource"], $rmsg);
            if($oksend){
                $this->db->deleteMessage($value["id"]);
            } else {
                $this->disconnect($data);
                return;
            }
        }
    }
    
    /**
     * Send Message to other user
     *
     * @param {Mixed} Data of user
     * @param {Mixed} Msg to process
     *
     */
    private function msg($data, $msg){
        if(!isset($msg["tokenuser"], $msg["tokenpass"], $msg["userfrom"], $msg["data"])){
            $this->disconnect($data);
            return;
        }
        $msg["userfrom"] = strtolower($msg["userfrom"]);
        if(!preg_match('/^[A-Za-z0-9]{8}$/', $msg["tokenuser"]) || !preg_match('/^[A-Za-z0-9]{32}$/', $msg["tokenpass"]) || !preg_match('/^[a-z0-9-_]{5,40}$/', $msg["userfrom"])){
            $this->disconnect($data);
            return;
        }
        
        $check = $this->db->checkSend($msg["tokenuser"], $msg["tokenpass"], $data["uname"], $msg["userfrom"]);
        if ($check === "failed"){
            $faile = ["type" => "sys", "error" => "user not found"];
            $this->socket->sendUsr($data["resource"], $faile);
            return;
        }else if ($check === false){
            $faile = ["type" => "sys", "error" => "user not found"];
            $this->socket->sendUsr($data["resource"], $faile);
            $this->disconnect($data);
            return;
        }
        
        $smsg = ["type" => $msg["type"],
                "username" => $data["uname"],
                "data" => $msg["data"]];
        $userfrom = $this->socket->usersocket($msg["userfrom"]);
        $this->socket->sendUsr($data['resource'], ["sys" => "send"]);
        if ($userfrom === false){
            $this->db->saveMessage($data["uname"], $msg["userfrom"], $msg["data"], ($msg["type"] == "cert"));
            return;
        }
        $this->socket->sendUsr($userfrom, $smsg);
    }

    /**
     * Disconnect user of websocket
     *
     * @param {Mixed} Data of user
     * @param {Mixed} Msg to process
     *
     */
    private function disconnect($data){
        $this->socket->logout($data['resource']);
    }
}
?>