<?php
class socket{
    private $sockets = [];
    private $master;

    /**
     * Create Object WebSocket
     *
     * @param {Integer}
     * @param {Integer} Number of port
     *
     */
    public function __construct($host, $port){
        $this->master = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_option($this->master, SOL_SOCKET, SO_REUSEADDR, 1);
        socket_bind($this->master, $host, $port);
        socket_listen($this->master, SOMAXCONN);
        $this->sockets[0] = ['resource' => $this->master, 'uname' => ''];
    }

    /**
     * Send message to user
     *
     * @param {Mixed} Socket to send
     * @param {String} Msg to send
     *
     * @return {Boolean} If msg is send correctly or not
     *
     */
    public function sendUsr($socket, $msg){
        $mask = $this->mask($msg);
        $length = strlen($mask);
        $sent = @socket_write($socket, $mask, $length);
        if ($sent === false) return false;
        if ($sent !== $length) return false;
        return true;
    }

    /**
     * Get resource user is connect
     *
     * @param {String} Msg to Send
     *
     * @return {Mixed} Socket if exist else false
     *
     */
    public function userSocket($username){
        foreach ($this->sockets as $socket) {
            if ($socket["uname"] == $username) return $socket['resource'];
        }
        return false;
    }

    /**
     * Get new messages of clients
     *
     * @return {Mixed} Array of new messages
     *
     */
    public function getMessages(){
        $sockets = array_column($this->sockets, 'resource');
        $write = $except = NULL;
        socket_select($sockets, $write, $except, NULL);

        $msg = [];

        foreach ($sockets as $socket) {
            if($socket == $this->master){
                $client = socket_accept($socket);
                if ($client !== false) {
                    $socket_info = [
                        'resource' => $client,
                        'uname' => '',
                        'handshake' => false
                    ];
                    $this->sockets[(int)$client] = $socket_info;
                }
            } else {
                if ($this->sockets[(int)$socket]['handshake']) {
                    
                    $longf = @socket_recv($socket, $buffer, 2048, 0);

                    if ($longf === false) break;
                    $optcode = ord($buffer[0]) & 15;

                    if($optcode == 8 || $longf == 0){
                        $this->logout($socket);
                        continue;
                    }

                    $long = (ord($buffer[1]) & 127)+6;
                    if ($long == 132) $long = $this->toNumber(substr($buffer, 2, 2)) + 2;
                    else if ($long == 133) $long = $this->toNumber(substr($buffer, 2, 8)) + 8;

                    $reclong = $long - $longf;

                    if($long>52428800){
                        $this->logout($socket);
                        continue;
                    }

                    while ($reclong>0) {
                        $longf = @socket_recv($socket, $buffertemp, 2048, 0);
                        $buffer .= $buffertemp;
                        $reclong -= $longf;
                    }

                } else {
                    $long = @socket_recv($socket, $buffer, 2048, 0);
                    $parse = $this->hand($buffer);
                    if ($parse === false) {
                        $this->logout($socket);
                        continue;
                    }
                    array_push($msg, ["user" => $this->sockets[(int)$socket], "msg" => $parse]);
                    continue;
                }
                array_push($msg, ["user" => $this->sockets[(int)$socket], "msg" => $this->unmask($buffer)]);
            }
        }
        return $msg;
    }

    /**
     * Logout one user
     *
     * @param {Mixed} Socket to disconnect
     *
     */
    public function logout($socket){
        socket_close($socket);
        unset($this->sockets[(int)$socket]);
    }

    /**
     * Unmasked String
     *
     * @param {String} Buffer of first bytes
     * @param {Mixed} Object to read new String
     *
     * @return {Mixed} Array of data
     *
     */
    private function unmask($buffer) {
        $decoded = '';
        $len = ord($buffer[1]) & 127;

        if ($len === 126) {
            $masks = substr($buffer, 4, 4);
            $data = substr($buffer, 8);
        } else if ($len === 127) {
            $masks = substr($buffer, 10, 4);
            $data = substr($buffer, 14);
        } else {
            $masks = substr($buffer, 2, 4);
            $data = substr($buffer, 6);
        }
        for ($index = 0; $index < strlen($data); $index++) {
            $decoded .= $data[$index] ^ $masks[$index % 4];
        }
        return json_decode($decoded, true);
    }

    /**
     * Masked String
     *
     * @param {String} Message to convert to Binary
     *
     * @return {String} Binary data
     *
     */
    private function mask($msg) {
        $msg = json_encode($msg);
        $frame = [];
        $frame[0] = '81';
        $len = strlen($msg);
        if ($len < 126) {
            $frame[1] = $len < 16 ? '0' . dechex($len) : dechex($len);
        } else if ($len < 65025) {
            $s = dechex($len);
            $frame[1] = '7e' . str_repeat('0', 4 - strlen($s)) . $s;
        } else {
            $s = dechex($len);
            $frame[1] = '7f' . str_repeat('0', 16 - strlen($s)) . $s;
        }

        $data = '';
        $l = strlen($msg);
        for ($i = 0; $i < $l; $i++) {
            $data .= dechex(ord($msg{$i}));
        }
        $frame[2] = $data;

        $data = implode('', $frame);

        return pack("H*", $data);
    }

    /**
     * Parse handshake
     *
     * @param {String} Data received
     *
     * @return {Boolean} If it done is true else false
     *
     */
    public function hand($buffer) {
        $val = preg_match('/^GET \/.*\?(.*) HTTP\//', $buffer, $str);
        if ($val !== 1) return false;
        parse_str($str[1], $get);
        if(!isset($get["tokenuser"], $get["tokenpass"])) return false;
        
        $check = strpos($buffer, 'Sec-WebSocket-Key:');
        if ($check === false) return false;
        $line_with_key = substr($buffer, $check + 18);
        $key = trim(substr($line_with_key, 0, strpos($line_with_key, "\r\n")));
        if (strlen(base64_decode($key)) !== 16) return false;
        $ret = ["tokenuser" => $get["tokenuser"],
                "tokenpass" => $get["tokenpass"],
                "key" => $key];
                
        return $ret;
    }
    
    /**
     * Response handshake login in app
     *
     * @param {Mixed} Socket to do handshake
     * @param {String} Data received
     *
     * @return {Boolean} If it done is true else false
     *
     */
    public function shake($socket, $user, $key) {
        $upgrade_key = base64_encode(sha1($key . "258EAFA5-E914-47DA-95CA-C5AB0DC85B11", true));
        $upgrade_message = "HTTP/1.1 101 Switching Protocols\r\n";
        $upgrade_message .= "Upgrade: websocket\r\n";
        $upgrade_message .= "Sec-WebSocket-Version: 13\r\n";
        $upgrade_message .= "Connection: Upgrade\r\n";
        $upgrade_message .= "Sec-WebSocket-Accept:" . $upgrade_key . "\r\n\r\n";
        
        @socket_write($socket, $upgrade_message, strlen($upgrade_message));
        $this->sockets[(int)$socket]['handshake'] = true;
        $this->sockets[(int)$socket]['uname'] = $user;

        $msg = ["type" => "handshake", "content" => "done"];

        $msg = $this->mask($msg);
        @socket_write($socket, $msg, strlen($msg));
        return true;
    }

    /**
     * Convert String to Integer
     *
     * @param {String} String to convert
     *
     * @return {Integer} Integer converted
     *
     */
    private function toNumber($bytes){
        $n = 0;
        for ($i=0; $i<strlen($bytes); $i++) {
            $n = ($n << 8) ^ ord($bytes[$i]);
        }
        return $n;
    }

    /**
     * Close Websocket
     *
     */
    public function __destruct(){
        socket_close($this->master);
    }
}
?>